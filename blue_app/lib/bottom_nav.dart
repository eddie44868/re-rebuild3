import 'package:blue_app/home.dart';
import 'package:blue_app/profile.dart';
import 'package:flutter/material.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({ Key? key }) : super(key: key);

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
int _selectedIndex = 0;
final halaman = [
  Home(),
  Home(),
  Home(),
  Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: halaman[_selectedIndex],
      floatingActionButton: FloatingActionButton(
      onPressed: () {},
      backgroundColor: Colors.blue[900],
      child: Center(
        child: Icon(Icons.add),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        shape: CircularNotchedRectangle(),
        notchMargin: 4, 
        child: Padding(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  IconButton(
                    icon: Column(
                    children: [
                      Image.asset('assets/home.png', height: 20, width: 20, color: Colors.blue[900]),
                      Text('Home', style: TextStyle(fontSize: 8.5, color: Colors.blue[900]),)
                    ],
                  ), 
                    onPressed: () {
                      setState(() {
                        _selectedIndex = 0;
                      });
                    },
                  ),
                  SizedBox(width: 20,),
                  IconButton(
                    icon: Column(
                    children: [
                      Image.asset('assets/list.png', height: 20, width: 20, color: Colors.blue[900]),
                      Text('My Task', style: TextStyle(fontSize: 8.5, color: Colors.blue[900]),)
                    ],
                  ), 
                    onPressed: () {
                      setState(() {
                        _selectedIndex = 1;
                      });
                    },
                  ),
                ],
              ),
              Row(
                children: [
                  IconButton(
                    icon: Column(
                    children: [
                      Image.asset('assets/dollar.png', height: 20, width: 20, color: Colors.blue[900]),
                      Text('Payment', style: TextStyle(fontSize: 8.1, color: Colors.blue[900]),)
                    ],
                  ), 
                    onPressed: () {
                      setState(() {
                        _selectedIndex = 2;
                      });
                    },
                  ),
                  SizedBox(width: 20,),
                  IconButton(
                    icon: Column(
                    children: [
                      Image.asset('assets/profile.png', height: 20, width: 20, color: Colors.blue[900]),
                      Text('Profile', style: TextStyle(fontSize: 8.5, color: Colors.blue[900]),)
                    ],
                  ), 
                    onPressed: () {
                      setState(() {
                        _selectedIndex = 3;
                      });
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
