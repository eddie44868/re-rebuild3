import 'package:blue_app/widget/box.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int num = 0;
  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Padding(
          padding: EdgeInsets.only(top: 10,),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset('assets/category.png', height: 30, width: 30, color: Colors.blue[900]),
              Text('Home', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.blue[900])),
              Image.asset('assets/bell.png', height: 30, width: 30, color: Colors.blue[900])
            ],
          ),
        )
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Padding(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 15),
              Text('Hi Jenifer!', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.blue[900])),
              Text('Good Morning', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.grey[400])),
              SizedBox(height: 25),
              Center(
                child: Container(
                  height: 50,
                  width: widthScreen-30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.grey[200]
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(left: 15,),
                    child: Row(
                      children: [
                        Icon(Icons.search, size: 35, color: Colors.blue[900]),
                        SizedBox(width: 15),
                        Text('Search', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: Colors.blue[900])),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Center(
                child: Container(
                  height: 120,
                  width: widthScreen-30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 2, color: Colors.blue.shade900),
                    color: Colors.grey[100]
                  ),
                    child: Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('Welcome!', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.blue[900])),
                              SizedBox(height: 5),
                              Text("Let's schedule your\nprojects", style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.grey[700])),
                            ],
                          ),
                        Image.asset('assets/Development.png', height: 100, width: 100,)
                      ],
                    ),
                  ),
                  ),
                ),
              SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Ongoing Projects", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.blue[900])),
                  Text("View  All", style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black38)),
                ],
              ),
              SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        num = 0;
                      });
                    },
                    child: Box(
                      imageUrl: 'assets/pc.png', 
                      text: 'Mobile App', 
                      text2: "E-Commerce", 
                      value: 0.5, 
                      text3: '50%', 
                      warna: (num == 0) ? Colors.blue.shade900 : Colors.blue.shade50, 
                      teks: (num == 0) ? Colors.white : Colors.blue.shade900,),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        num = 1;
                      });
                    },
                    child: Box(
                      imageUrl: 'assets/menu.png', 
                      text: 'Dashboard', 
                      text2: "Hotel", 
                      value: 0.5, 
                      text3: '50%', 
                      warna: (num == 1) ? Colors.blue.shade900 : Colors.blue.shade50,
                      teks: (num == 1) ? Colors.white : Colors.blue.shade900,),),
                ],
              ),
              SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        num = 2;
                      });
                    },
                    child: Box(
                      imageUrl: 'assets/megaphone.png', 
                      text: 'Banner', 
                      text2: "Marketing", 
                      value: 0.4, 
                      text3: '40%', 
                      warna: (num == 2) ? Colors.blue.shade900 : Colors.blue.shade50, 
                      teks: (num == 2) ? Colors.white : Colors.blue.shade900,),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        num = 3;
                      });
                    },
                    child: Box(
                      imageUrl: 'assets/creativity.png', 
                      text: 'UI/UX', 
                      text2: "Task Manager", 
                      value: 0.6, 
                      text3: '60%', 
                      warna: (num == 3) ? Colors.blue.shade900 : Colors.blue.shade50,
                      teks: (num == 3) ? Colors.white : Colors.blue.shade900,),),
                ],
              ),
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}