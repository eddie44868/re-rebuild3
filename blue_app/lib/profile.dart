import 'package:blue_app/widget/box2.dart';
import 'package:blue_app/widget/folder.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(left: 15, right: 15, top: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                    height: 300,
                    width: widthScreen-30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.blue[900]
                    ),
                    child: Center(
                      child: Column(
                        children: [
                          SizedBox(height: 25),
                          Image.asset('assets/Picture1.png', height: 100, width: 100,),
                          SizedBox(height: 15),
                          Text("Putra Editia", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white),),
                          Text("Mobile App Developer", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300, color: Colors.white),),
                          SizedBox(height: 35),
                          Padding(
                            padding: EdgeInsets.only(left: 30, right: 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  children: [
                                    Text("75K", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                                    Text("Followers", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300, color: Colors.white),),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Text("16K", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                                    Text("Followings", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300, color: Colors.white),),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Text("600", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                                    Text("Projects", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300, color: Colors.white),),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    )
                  ),
                ),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Folders", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.blue[900])),
                    Text("View  All", style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black38)),
                  ],
                ),
                SizedBox(height: 15),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Folder(text: 'Dribble Shorts',),
                      Folder(text: 'Icons',),
                    ],
                  ),
                ),
                SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("My Team", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.blue[900])),
                    Text("View  All", style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black38)),
                  ],
                ),
                SizedBox(height: 25),
                Box2(imageUrl: 'assets/bag.png', foto1: 'assets/Daco1.png', foto2: 'assets/Daco2.png', text: "E-Commerce Apllication", text2: "Project in progress"),
                Box2(imageUrl: 'assets/hotel-bell.png', foto1: 'assets/Daco3.png', foto2: 'assets/Daco4.png', text: "Food Project", text2: "Completed"),
                Box2(imageUrl: 'assets/book.png', foto1: 'assets/Daco5.png', foto2: 'assets/Daco6.png', text: "E-Book Project", text2: "Project in progress"),
                Box2(imageUrl: 'assets/bag.png', foto1: 'assets/Daco7.png', foto2: 'assets/Daco8.png', text: "E-Learning Mobile App", text2: "Completed"),
                SizedBox(height: 30),
              ],
            ),
          )
        ),
      )
    );
  }
}