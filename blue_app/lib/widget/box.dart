import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Box extends StatelessWidget {
  String imageUrl;
  String text;
  String text2;
  double value;
  String text3;
  Color warna;
  Color teks;
  Box({ Key? key, required this.imageUrl, required this.text, required this.text2, required this.value, required this.text3, required this.warna, required this.teks}) : super(key: key);
//Colors.blue[900]
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160,
      width: 160,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: warna
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 15, right: 15, top: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("May 30, 2022", style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal, color: teks)),
                Image.asset('assets/options.png', height: 10, width: 10, color: teks),
              ],
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(imageUrl, height: 30, width: 30, color: teks),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(text, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: teks)),
                    SizedBox(height: 5),
                    Text(text2, style: TextStyle(fontSize: 10, fontWeight: FontWeight.normal, color: teks)),
                  ],
                ),
                SizedBox(width: 2,)
              ],
            ),
            SizedBox(height: 15),
            Text("Progress", style: TextStyle(fontSize: 10, fontWeight: FontWeight.normal, color: teks)),
            SizedBox(height: 10),
            LinearProgressIndicator(
              color: teks,
              value: value,
              backgroundColor: Colors.white70,
            ),
            SizedBox(height: 5),
            Align(
              alignment: Alignment.centerRight,
              child: Text(text3, style: TextStyle(fontSize: 10, fontWeight: FontWeight.normal, color: teks),),
            ),
          ],
        ),
      ),
    );
  }
}