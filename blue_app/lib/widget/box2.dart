import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Box2 extends StatelessWidget {
  String imageUrl;
  String foto1;
  String foto2;
  String text;
  String text2;
  Box2({ Key? key, required this.imageUrl, required this.foto1, required this.foto2, required this.text, required this.text2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Column(
      children: [
        Container(
          height: 70,
          width: widthScreen-40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.grey[100]
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.blue[50]
                    ),
                    child: Center(
                      child: Image.asset(imageUrl, height: 30, width: 30, color: Colors.blue[900],),
                    ),
                  ),
                  SizedBox(width: 20),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(text, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.blue[900])),
                      Text(text2, style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Colors.black38)),
                    ],
                  ),
                ],
              ),
              Stack(
                children: [
                  Image.asset(foto1, height: 30, width: 30,),
                  Row(
                    children: [
                      SizedBox(width: 20),
                      Image.asset(foto2, height: 30, width: 30,),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
        SizedBox(height: 10,)
      ],
    );
  }
}