import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Folder extends StatelessWidget {
  String text;
  Folder({ Key? key, required this.text }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Row(
      children: [
        Container(
          height: 90,
          width: widthScreen-100,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.blue[50]
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 25, top: 25, right: 80),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset('assets/folder.png', height: 40, width: 40, color: Colors.blue[900],),
                Column(
                  crossAxisAlignment:   CrossAxisAlignment.start,
                  children: [
                    Text(text, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.blue[900])),
                    Text("Created: 28 Feb 2022", style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Colors.black38)),
                    Stack(
                      children: [
                        Image.asset('assets/Daco1.png', height: 20, width: 20,),
                        Row(
                          children: [
                            SizedBox(width: 15),
                            Image.asset('assets/Daco2.png', height: 20, width: 20,),
                          ],
                        ),
                        Row(
                          children: [
                            SizedBox(width: 30),
                            Image.asset('assets/Daco3.png', height: 20, width: 20,),
                          ],
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        SizedBox(width: 15,)
      ],
    );
  }
}